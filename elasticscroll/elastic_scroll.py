# -*- coding: utf-8 -*-
import requests
import json

class ElasticScrollQuery(object):
    def __init__(self, observer, url, query, scroll_dt):
        self.scroll_dt = scroll_dt
        self.scroll_id = None
        self.query = json.dumps(query)
        assert url.endswith('_search'), "URL should end with '_search'"
        self.url = url
        self.observer = observer

    def exec_query(self):
        r = requests.get(url=self.url + '?scroll={}'.format(self.scroll_dt),
                         data=self.query,
                         headers={'Content-Type': 'application/json'})
        r = r.json()

        # get first scroll id
        self.scroll_id = r['_scroll_id']

        data = []
        # process first batch
        for hit in r['hits']['hits']:
            elastic_id = hit['_id']
            src = hit['_source']
            src['elastic_id'] = elastic_id
            data.append(src)

        if data:
            self.observer.received_data(data)

        scroll_size = len(r['hits']['hits'])

        while scroll_size > 0:
            body = json.dumps(dict(scroll_id=self.scroll_id, scroll=self.scroll_dt))
            r = requests.get(url=self.url + '/scroll',
                             data=body,
                             headers={'Content-Type': 'application/json'})
            r = r.json()
            self.scroll_id = r['_scroll_id']
            scroll_size = len(r['hits']['hits'])

            data = []
            # process next batch
            for hit in r['hits']['hits']:
                elastic_id = hit['_id']
                src = hit['_source']
                src['elastic_id'] = elastic_id
                data.append(src)

            if data:
                self.observer.received_data(data)



class ELSObserver(object):
    def __init__(self):
        self.data = []

    def received_data(self, d):
        self.data.append(d)


if __name__ == '__main__':
    pass